$(document).ready(function() {
    $('#image-1').rcrop({
        minSize: [200, 200],
        preserveAspectRatio: true,
        grid: true,

        preview: {
            display: true,
            size: [100, 100],
        }
    });

});
